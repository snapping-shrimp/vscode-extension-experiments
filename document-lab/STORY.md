# Story

https://gitlab.com/snapping-shrimp/vscode-extension-experiments/-/issues/8
https://gitlab.com/snapping-shrimp/cocreate-remote-vscode/-/issues/47

So, we are going to experiment with documents in VS Code. These documents will
be created programmatically, in a new window or in the same window where the
user is already present

https://code.visualstudio.com/api/references/vscode-api

Check this, there's something called a CustomDocument

https://code.visualstudio.com/api/references/vscode-api#CustomDocument

Custom Document is used by Custom Editors

https://code.visualstudio.com/api/references/vscode-api#CustomEditorProvider

For simple text documents like in our case, we can go ahead and use Text
Document, which is used by the Custom Text Editor and also by the default one
I think

https://code.visualstudio.com/api/references/vscode-api#CustomTextEditorProvider

https://code.visualstudio.com/api/references/vscode-api#TextDocument

I'm thinking about not using anything custom and still try to show text
documents programmatically. Hmm.

https://code.visualstudio.com/api/references/vscode-api#TextEditor

I'm wondering how to create a `TextDocument` and if it's even possible. Hmm

And there's a way to show a `TextDocument` using window

https://code.visualstudio.com/api/references/vscode-api#window

`showTextDocument` method

https://code.visualstudio.com/api/extension-guides/virtual-documents

I was trying out the example extension present in the virtual documents web page

https://github.com/microsoft/vscode-extension-samples/blob/master/virtual-document-sample/README.md

It had a good example of a read only document. I'm still not able to find how
to easily create an editor with some text in it and in a read only manner. Hmm.

Given that mine is kind of like a virtual document - it's not present in the
file system - more like, the source of truth is partially on the other user's
computer too. Both area kind of like source of truth in a collaborative world.
Hmm.

And damn it. GitHub authentication sample is clearly present here

https://github.com/microsoft/vscode-extension-samples/tree/master/github-authentication-sample

Hmm. There's also commenting sample, hmmm
https://github.com/microsoft/vscode-extension-samples/tree/master/comment-sample

Nice!

Imma check all sample names. Just a glance.

https://github.com/microsoft/vscode-extension-samples/tree/master/fsprovider-sample
https://github.com/microsoft/vscode-extension-samples/tree/master/document-editing-sample

---

V1.51 has been released and I was checking the new features for extension
authors

https://code.visualstudio.com/updates/v1_51
https://code.visualstudio.com/updates/v1_51#_extension-authoring

I also noticed and properly read about Proposed API.

https://code.visualstudio.com/updates/v1_51#_proposed-extension-apis
https://github.com/microsoft/vscode/blob/master/src/vs/vscode.proposed.d.ts
https://code.visualstudio.com/api/advanced-topics/using-proposed-api

Wow! They have an API for storing passwords in the proposed API!! Wow!!

https://code.visualstudio.com/updates/v1_51#_password-apis

And it's of course a wrapper around keytar

https://github.com/atom/node-keytar

Hmm. Nice!

Something to look out for

https://github.com/microsoft/vscode/issues/88309

I tried to show an untitled editor tab, but couldn't. It did say that the tab
was open and even on the left I could see notification in file explorer but I
couldn't see any tab being open. Weird

```typescript
async () => {
  try {
    let texdocument = await vscode.workspace.openTextDocument({
      content: "sample text content",
      language: "text",
    });
    console.log(texdocument.fileName);
  } catch (e) {
    console.error(e);
  }
};
```

I also tried to open a new windows / new workspace / new instance of VS Code for
the future but couldn't do it exactly.

I think I can try one of the following - file system provider , custom editor
provider, custom text document content provider

---

`vscode.workspace.registerTextDocumentContentProvider`

Example -
https://github.com/microsoft/vscode-extension-samples/tree/master/virtual-document-sample

---

File System Provider. Mem based file system etc. Example -

https://github.com/microsoft/vscode-extension-samples/tree/master/fsprovider-sample

---

Custom Editor Provider - Usually for non-text files I think

`vscode.window.registerCustomEditorProvider`

https://github.com/microsoft/vscode-extension-samples/tree/master/custom-editor-sample

---

Maybe for the future, mem fs looks nice. But I still need to understand how to
create a new window with a workspace opened in it. And mem fs, not sure about
it till I dig more into it. But I think I don't want to implement the whole mem
fs myself. I could use some good library. Hmm. It could also be based on a temp
file / directory in `/tmp`

With `TextDocumentContentProvider`, I'm afraid we will have a readonly document
and cannot change it even programatically to show live changes from remote user.
Hmm.

I think it's still okay. We can start off with a static read only document too.
Hmm. And think about the networking side of things. And then come back to this.
There are other ways to try out things too. Hmm. I know for sure that we don't
need a static read only document where content comes only the first time. Or may
be I could change content by doing something. But I wonder how it will look like
and not sure about performance. Gotta first try this out. Only then I'll know
how much control is there in this case. And then I can move on to networking
soon and try to solve it and then come back to this. Hmm.

Or, I could move to networking and get back to this. I can do an analysis on the
networking part. Hmm.

