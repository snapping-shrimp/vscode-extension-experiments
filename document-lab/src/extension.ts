import * as vscode from "vscode";

export function activate(context: vscode.ExtensionContext) {
  console.log('Congratulations, your extension "document-lab" is now active!');

  let disposable = vscode.commands.registerCommand(
    "document-lab.openDocInWindow",
    async () => {
      try {
        let texdocument = await vscode.workspace.openTextDocument({
          content: "sample text content",
          language: "text",
        });
        console.log(texdocument.fileName);
      } catch (e) {
        console.error(e);
      }
    }
  );

  const myScheme = "document-lab";
  const myProvider = new (class implements vscode.TextDocumentContentProvider {
    provideTextDocumentContent(uri: vscode.Uri): string {
      // simply invoke cowsay, use uri-path as text
      // return `This is a sample file - ${uri.path}`;
      return `console.log("This is great!");`;
    }
  })();

  context.subscriptions.push(
    vscode.workspace.registerTextDocumentContentProvider(myScheme, myProvider)
  );

  // register a command that opens a cowsay-document
  context.subscriptions.push(
    vscode.commands.registerCommand("document-lab.readonly-doc", async () => {
      const what = await vscode.window.showInputBox({
        placeHolder: "Type file name",
      });
      if (what) {
        const uri = vscode.Uri.parse(`${myScheme}:${what}.ts`);
        const doc = await vscode.workspace.openTextDocument(uri); // calls back into the provider
        await vscode.window.showTextDocument(doc, { preview: false });
      }
    })
  );

  context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}
