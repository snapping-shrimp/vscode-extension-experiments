# Story

This sample example looks related!

https://github.com/microsoft/vscode-extension-samples/tree/master/decorator-sample

Okay, so I ran it. I can see it decorate the text in the document, and also show
some content on hover and also change the cursor style - kind, on hover. It
wasn't exactly a colored cursor. Gotta see how to create a colored cursor and
how to create a tool tip instead of a big box below. I just noticed that it
shows the box below for the first line content, but for any other line, it
shows above only, so, not so bad ;) :D It's appropriate I guess.

I'm reading the one and only typescript file in the repo. I have copied all of
the files into this repo.

I can see how they do decorations using `createTextEditorDecorationType`

I tried to run to understand what the code is trying to do by keeping the
extension running parallely.

Weirdly, I couldn't run `npm run watch`. I think it's key to run it because it
converts the typescript files to javascript which is then used to run the
extension - I think. The issue was something related to `tsc` not being found.
I had copied the node_modules too. Maybe something went wrong there. I didn't
check what.

I had to remove the node_modules and reinstall with `npm install` and then
`npm run watch` worked! :)

I finally understood what `overviewRulerLane` and `overviewRulerColor` is.
In the scrollbar, you can see some decorations when using VS Code. That's the
overview ruler - the scrollbar's inside. When I click on a word, it shows me
where all the word is present with a colored box and the box appears in the
center - that's the lane. There's

```javascript
export enum OverviewRulerLane {
	Left = 1,
	Center = 2,
	Right = 4,
	Full = 7
}
```

Nice! But this is not exactly going to help me now with my cursor. Also, the
color is the color of the box, hence `overviewRulerColor`.

There are some default styles for some of this stuff already - the grey box or
whatever colored box, based on the theme, and it appearing on the center, to
show occurences of the word. `Left`, `Right`, `Center` is clear. I think `Full`
is what is used for showing where the cursor is. Currently I can see a default
`Full` box in my overview ruler for my cursor's place. Yes, the `Full` is
supposed to mean fully covering the lane - a full width box. Others are covering
only parts of it - left, right or center. The height of the box is still
dependent on other things I guess :)

There's also decoration for light and dark themes - like, there's some
overriding for the colors based on the themes.

So, there are three kinds of themes - dark, light, and high contrast and then
there's tons of colors for these themes based on highlight of the cursor and
then based on the language features and what not.

It's nice that we can actually let the user choose the color if they want - we
can just provide some default values.

```javascript
vscode.window.createTextEditorDecorationType({
  cursor: "crosshair",
  // use a themable color. See package.json for the declaration and default values.
  backgroundColor: { id: "myextension.largeNumberBackground" },
});
```

In the `package.json`, there's this

```json
{
  "contributes": {
    "colors": [
      {
        "id": "myextension.largeNumberBackground",
        "description": "Background decoration color for large numbers",
        "defaults": {
          "dark": "#FF000055",
          "light": "#FF000055",
          "highContrast": "#FF000055"
        }
      }
    ]
  }
}
```

I was reading at how the decoration is done for the text. There's a lot of code
related to what is the active editor. Hmm. And then based on that, decorations
are triggered.

And then there is a regex to check for numbers and then the document text is
matched with the regex and then decorated by using positions (ranges) and then
along with that, there was a hover decoration too. It was decorated using a
decorate option.

The same stuff is also present in the USAGE.md file in the repo. Anyways.

I just found out about the cursor part of things -

https://code.visualstudio.com/api/references/vscode-api#TextEditorCursorStyle
near
https://code.visualstudio.com/api/references/vscode-api#TextEditorEdit

Text editor cursor style is also part of the options of a text editor

https://code.visualstudio.com/api/references/vscode-api#TextEditorOptions

I think that's how we can control it!

Hmm, so I tried it out. I'm only able to tell how the cursor looks like in
general, but not it's color or hover.

```javascript
activeEditor.options.cursorStyle = vscode.TextEditorCursorStyle.Underline;
```

```javascript
/**
	 * Rendering style of the cursor.
	 */
	export enum TextEditorCursorStyle {
		/**
		 * Render the cursor as a vertical thick line.
		 */
		Line = 1,
		/**
		 * Render the cursor as a block filled.
		 */
		Block = 2,
		/**
		 * Render the cursor as a thick horizontal line.
		 */
		Underline = 3,
		/**
		 * Render the cursor as a vertical thin line.
		 */
		LineThin = 4,
		/**
		 * Render the cursor as a block outlined.
		 */
		BlockOutline = 5,
		/**
		 * Render the cursor as a thin horizontal line.
		 */
		UnderlineThin = 6
	}
```

Currently I have started checking out existing extensions that do this. Can't
spend too much time on this 😅

https://marketplace.visualstudio.com/items?itemName=therealmarv.vscode-theme-dark-atom-dark-green-cursor - Green cursor it seems! :D

GitHub repo -
https://github.com/therealmarv/vscode-theme-dark-atom-dark-green-cursor

This is a theme though. Hmm. Gotta see what they do with themes. Maybe I can
insert some code for cursor color through themes? But I need multiple colors and
dynamic ones for different cursors. Hmm. Let's start with this one though.

In this repo, I can see XML document. Too big. And not able to find the green
or the cursor very easily. Gonna check how themes set cursor color, with XML.

Okay, looks like there is a lot of stuff on VS Code themes.

https://css-tricks.com/creating-a-vs-code-theme/
https://marketplace.visualstudio.com/items?itemName=sdras.night-owl&WT.mc_id=github-theme-sdras

Couldn't find cursor stuff there, so chucked it!

https://code.visualstudio.com/api/extension-capabilities/theming
https://code.visualstudio.com/api/extension-guides/color-theme

https://code.visualstudio.com/api/references/theme-color#editor-colors
https://code.visualstudio.com/api/references/theme-color#integrated-terminal-colors

Some cursor related stuff I saw

editorCursor.background: The background color of the editor cursor. Allows customizing the color of a character overlapped by a block cursor.

editorCursor.foreground: Color of the editor cursor.

terminalCursor.background: The background color of the terminal cursor. Allows customizing the color of a character overlapped by a block cursor.

terminalCursor.foreground: The foreground color of the terminal cursor.

https://code.visualstudio.com/api/references/contribution-points#contributes.themes

```json
{
  "contributes": {
    "themes": [
      {
        "label": "Monokai",
        "uiTheme": "vs-dark",
        "path": "./themes/monokai-color-theme.json"
      }
    ]
  }
}
```

There's also how to create a theme - a sample

https://github.com/microsoft/vscode-extension-samples/tree/master/theme-sample

There's also some information here

https://code.visualstudio.com/api/extension-guides/color-theme#create-a-new-color-theme

There's something based on textmate too. Anyways, I'm just going to try the
JSON format and see how it goes! :)

Cool! I was able to create a green colored cursor with ease!! :D And also
customize color for the case of block cursors - as block cursors will overlap
with characters - in which case, characters need some color. Not sure what
happens when no color is given for the background for the character.

Okay, I found it out - when no background is given, it somehow finds out a
contrast color or some color to show the character when the block cursor
overlaps with the character :) Like it showed red automatically when green was
the block cursor color. White for blue. Red for yellow. Nice!

Now, I need to know how I can create custom styled cursors as VS Code Live share
had a custom one I think. At least I need a dynamic coloring for the different
cursors I have. To start with, I gotta see if I can have dynamic color for one
cursor. Hmm.

Gotta see what can be done. I can see something called Custom Editors -

https://code.visualstudio.com/api/extension-guides/custom-editors

Complete customization I guess? Idk

---

I was checking a bit of custom editors.

Parallely, I checked about tooltip / hover and found many matches when searching
for the term "tooltip" here

https://code.visualstudio.com/api/references/vscode-api

And there's one thing called Hover
https://code.visualstudio.com/api/references/vscode-api#Hover

And then there's

https://code.visualstudio.com/api/references/vscode-api#Command

https://code.visualstudio.com/api/references/vscode-api#DocumentLink

https://code.visualstudio.com/api/references/vscode-api#EvaluatableExpression

https://code.visualstudio.com/api/references/vscode-api#QuickInputButton

https://code.visualstudio.com/api/references/vscode-api#SourceControlResourceDecorations

https://code.visualstudio.com/api/references/vscode-api#StatusBarItem

https://code.visualstudio.com/api/references/vscode-api#TerminalLink

https://code.visualstudio.com/api/references/vscode-api#TreeItem

---

```javascript
const position = activeEditor.document.positionAt(0)
const range = new vscode.Range(position, position)
const hover = new vscode.Hover("random thing", range)
```

But the hover thing didn't work. I didn't know how to give it to VS Code to show
it. I think it's a thing that's only by intellisense stuff or language server
protocols to show hovers in the editor
