# Story

I'm trying to create an extension with a name and try to run tests and see how
it goes on.

Things I want to understand

- What is the basic thing I can do for the extension? Is the basic hello world
  etc is enough?
- What is required to publish the extension? Unique ID for the extension etc?
- What does it mean to run integration tests? How to do it?
- Unit tests? Are there any existing ones?
- Lint? How to run?

I just tried out the integration test and it didn't run properly and wasn't
failing properly also.

```bash
$ npm run test

> karuppiah@0.0.1 pretest /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/vscode-extension-experiments/karuppiah
> npm run compile && npm run lint


> karuppiah@0.0.1 compile /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/vscode-extension-experiments/karuppiah
> tsc -p ./


> karuppiah@0.0.1 lint /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/vscode-extension-experiments/karuppiah
> eslint src --ext ts


> karuppiah@0.0.1 test /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/vscode-extension-experiments/karuppiah
> node ./out/test/runTest.js

Found .vscode-test/vscode-1.50.1. Skipping download.
Warning: 'sandbox' is not in the list of known options, but still passed to Electron/Chromium.

[main 2020-10-17T13:10:31.785Z] update#setState idle

nvm is not compatible with the "npm_config_prefix" environment variable: currently set to "/Users/karuppiahn/.nvm/versions/node/v12.19.0"

Run `unset npm_config_prefix` to unset it.

(node:74049) Electron: Loading non-context-aware native module in renderer: '/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/vscode-extension-experiments/karuppiah/.vscode-test/vscode-1.50.1/Visual Studio Code.app/Contents/Resources/app/node_modules.asar.unpacked/vscode-sqlite3/build/Release/sqlite.node'. This is deprecated, see https://github.com/electron/electron/issues/18397.

2020-10-17 18:40:32.700 Code Helper (Renderer)[74049:1142592] CoreText note: Client requested name ".NewYork-Regular", it will get Times-Roman rather than the intended font. All system UI font access should be through proper APIs such as CTFontCreateUIFontForLanguage() or +[NSFont systemFontOfSize:].
2020-10-17 18:40:32.700 Code Helper (Renderer)[74049:1142592] CoreText note: Set a breakpoint on CTFontLogSystemFontNameRequest to debug.

2020-10-17 18:40:32.707 Code Helper (Renderer)[74049:1142592] CoreText note: Client requested name ".NewYork-Regular", it will get Times-Roman rather than the intended font. All system UI font access should be through proper APIs such as CTFontCreateUIFontForLanguage() or +[NSFont systemFontOfSize:].

(node:74049) Electron: Loading non-context-aware native module in renderer: '/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/vscode-extension-experiments/karuppiah/.vscode-test/vscode-1.50.1/Visual Studio Code.app/Contents/Resources/app/node_modules.asar.unpacked/spdlog/build/Release/spdlog.node'. This is deprecated, see https://github.com/electron/electron/issues/18397.

(node:74242) [DEP0005] DeprecationWarning: Buffer() is deprecated due to security and usability issues. Please use the Buffer.alloc(), Buffer.allocUnsafe(), or Buffer.from() methods instead.

(node:74252) Electron: Loading non-context-aware native module in renderer: '/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/vscode-extension-experiments/karuppiah/.vscode-test/vscode-1.50.1/Visual Studio Code.app/Contents/Resources/app/node_modules.asar.unpacked/spdlog/build/Release/spdlog.node'. This is deprecated, see https://github.com/electron/electron/issues/18397.

Failed to connect to provider "No token found for host bitbucket.org"

[main 2020-10-17T13:11:01.785Z] update#setState checking for updates
```

Initially it even opened up 4 instances of VS Code. Also, apparently I can't
have even one instance running to run the tests from command line. What does
that even mean? I closed everything and then had to run the test. And it's
possible to run integration test from VS Code?? Hmm

`npm run test` runs `node ./out/test/runTest.js`

The file at `./src/test/runTest.js` seems to be the source file! So, going to
check that!!

Also, there's a `pretest` defined as `npm run compile && npm run lint`. So, this
is to run the `compile` task which will compile the TypeScript files into
JavaScript files and then do the linting with `lint`. Cool, makes sense!

The compilation is done with `tsc` like this `tsc -p ./`

Trying to figure out what that is

```bash
  $ npx tsc --help
Version 4.0.3
Syntax:   tsc [options] [file...]

Examples: tsc hello.ts
          tsc --outFile file.js file.ts
          tsc @args.txt
          tsc --build tsconfig.json

Options:
 -h, --help                                         Print this message.
 -w, --watch                                        Watch input files.
 --pretty                                           Stylize errors and messages using color and context (experimental).
 --all                                              Show all compiler options.
 -v, --version                                      Print the compiler's version.
 --init                                             Initializes a TypeScript project and creates a tsconfig.json file.
 -p FILE OR DIRECTORY, --project FILE OR DIRECTORY  Compile the project given the path to its configuration file, or to a folder with a 'tsconfig.json'.
 -b, --build                                        Build one or more projects and their dependencies, if out of date
 -t VERSION, --target VERSION                       Specify ECMAScript target version: 'ES3' (default), 'ES5', 'ES2015', 'ES2016', 'ES2017', 'ES2018', 'ES2019', 'ES2020', or 'ESNEXT'.
 -m KIND, --module KIND                             Specify module code generation: 'none', 'commonjs', 'amd', 'system', 'umd', 'es2015', 'es2020', or 'ESNext'.
 --lib                                              Specify library files to be included in the compilation.
                                                      'es5' 'es6' 'es2015' 'es7' 'es2016' 'es2017' 'es2018' 'es2019' 'es2020' 'esnext' 'dom' 'dom.iterable' 'webworker' 'webworker.importscripts' 'scripthost' 'es2015.core' 'es2015.collection' 'es2015.generator' 'es2015.iterable' 'es2015.promise' 'es2015.proxy' 'es2015.reflect' 'es2015.symbol' 'es2015.symbol.wellknown' 'es2016.array.include' 'es2017.object' 'es2017.sharedmemory' 'es2017.string' 'es2017.intl' 'es2017.typedarrays' 'es2018.asyncgenerator' 'es2018.asynciterable' 'es2018.intl' 'es2018.promise' 'es2018.regexp' 'es2019.array' 'es2019.object' 'es2019.string' 'es2019.symbol' 'es2020.bigint' 'es2020.promise' 'es2020.string' 'es2020.symbol.wellknown' 'es2020.intl' 'esnext.array' 'esnext.symbol' 'esnext.asynciterable' 'esnext.intl' 'esnext.bigint' 'esnext.string' 'esnext.promise'
 --allowJs                                          Allow javascript files to be compiled.
 --jsx KIND                                         Specify JSX code generation: 'preserve', 'react-native', or 'react'.
 -d, --declaration                                  Generates corresponding '.d.ts' file.
 --declarationMap                                   Generates a sourcemap for each corresponding '.d.ts' file.
 --sourceMap                                        Generates corresponding '.map' file.
 --outFile FILE                                     Concatenate and emit output to single file.
 --outDir DIRECTORY                                 Redirect output structure to the directory.
 --removeComments                                   Do not emit comments to output.
 --noEmit                                           Do not emit outputs.
 --strict                                           Enable all strict type-checking options.
 --noImplicitAny                                    Raise error on expressions and declarations with an implied 'any' type.
 --strictNullChecks                                 Enable strict null checks.
 --strictFunctionTypes                              Enable strict checking of function types.
 --strictBindCallApply                              Enable strict 'bind', 'call', and 'apply' methods on functions.
 --strictPropertyInitialization                     Enable strict checking of property initialization in classes.
 --noImplicitThis                                   Raise error on 'this' expressions with an implied 'any' type.
 --alwaysStrict                                     Parse in strict mode and emit "use strict" for each source file.
 --noUnusedLocals                                   Report errors on unused locals.
 --noUnusedParameters                               Report errors on unused parameters.
 --noImplicitReturns                                Report error when not all code paths in function return a value.
 --noFallthroughCasesInSwitch                       Report errors for fallthrough cases in switch statement.
 --types                                            Type declaration files to be included in compilation.
 --esModuleInterop                                  Enables emit interoperability between CommonJS and ES Modules via creation of namespace objects for all imports. Implies 'allowSyntheticDefaultImports'.
 @<file>                                            Insert command line options and files from a file.
```

So, `-p` means

```bash
-p FILE OR DIRECTORY, --project FILE OR DIRECTORY  Compile the project given the path to its configuration file, or to a folder with a 'tsconfig.json'.
```

So, it's pointing to the current directory.

And then there's linting with `eslint src --ext ts`

```bash
$ npx eslint --help
eslint [options] file.js [file.js] [dir]

Basic configuration:
  --no-eslintrc                   Disable use of configuration from .eslintrc.*
  -c, --config path::String       Use this configuration, overriding .eslintrc.* config options if present
  --env [String]                  Specify environments
  --ext [String]                  Specify JavaScript file extensions
  --global [String]               Define global variables
  --parser String                 Specify the parser to be used
  --parser-options Object         Specify parser options
  --resolve-plugins-relative-to path::String  A folder where plugins should be resolved from, CWD by default

Specifying rules and plugins:
  --rulesdir [path::String]       Use additional rules from this directory
  --plugin [String]               Specify plugins
  --rule Object                   Specify rules

Fixing problems:
  --fix                           Automatically fix problems
  --fix-dry-run                   Automatically fix problems without saving the changes to the file system
  --fix-type Array                Specify the types of fixes to apply (problem, suggestion, layout)

Ignoring files:
  --ignore-path path::String      Specify path of ignore file
  --no-ignore                     Disable use of ignore files and patterns
  --ignore-pattern [String]       Pattern of files to ignore (in addition to those in .eslintignore)

Using stdin:
  --stdin                         Lint code provided on <STDIN> - default: false
  --stdin-filename String         Specify filename to process STDIN as

Handling warnings:
  --quiet                         Report errors only - default: false
  --max-warnings Int              Number of warnings to trigger nonzero exit code - default: -1

Output:
  -o, --output-file path::String  Specify file to write report to
  -f, --format String             Use a specific output format - default: stylish
  --color, --no-color             Force enabling/disabling of color

Inline configuration comments:
  --no-inline-config              Prevent comments from changing config or rules
  --report-unused-disable-directives  Adds reported errors for unused eslint-disable directives

Caching:
  --cache                         Only check changed files - default: false
  --cache-file path::String       Path to the cache file. Deprecated: use --cache-location - default: .eslintcache
  --cache-location path::String   Path to the cache file or directory

Miscellaneous:
  --init                          Run config initialization wizard - default: false
  --env-info                      Output execution environment information - default: false
  --no-error-on-unmatched-pattern  Prevent errors when pattern is unmatched
  --debug                         Output debugging information
  -h, --help                      Show help
  -v, --version                   Output the version number
  --print-config path::String     Print the configuration for the given file
```

So, the `--ext` was to mention what extensions to lookout for - only `ts` in our
case! :) And the `src` is the source directory!

Coming back to the `runTest.ts` file!

Ah, seems to be a small and simple file

```typescript
import * as path from "path";

import { runTests } from "vscode-test";

async function main() {
  try {
    // The folder containing the Extension Manifest package.json
    // Passed to `--extensionDevelopmentPath`
    const extensionDevelopmentPath = path.resolve(__dirname, "../../");

    // The path to test runner
    // Passed to --extensionTestsPath
    const extensionTestsPath = path.resolve(__dirname, "./suite/index");

    // Download VS Code, unzip it and run the integration test
    await runTests({ extensionDevelopmentPath, extensionTestsPath });
  } catch (err) {
    console.error("Failed to run tests");
    process.exit(1);
  }
}

main();
```

It has a main function and then calls it. Apparently it's an asynchronous
function hmm.

```typescript
// The folder containing the Extension Manifest package.json
// Passed to `--extensionDevelopmentPath`
const extensionDevelopmentPath = path.resolve(__dirname, "../../");
```

`extensionDevelopmentPath` points to the extension's root directory.

And then there's `./suite/index` which is the extension tests path. A test
suite. This is also not that big. Good :P :)

```typescript
import * as path from "path";
import * as Mocha from "mocha";
import * as glob from "glob";

export function run(): Promise<void> {
  // Create the mocha test
  const mocha = new Mocha({
    ui: "tdd",
    color: true,
  });

  const testsRoot = path.resolve(__dirname, "..");

  return new Promise((c, e) => {
    glob("**/**.test.js", { cwd: testsRoot }, (err, files) => {
      if (err) {
        return e(err);
      }

      // Add files to the test suite
      files.forEach((f) => mocha.addFile(path.resolve(testsRoot, f)));

      try {
        // Run the mocha test
        mocha.run((failures) => {
          if (failures > 0) {
            e(new Error(`${failures} tests failed.`));
          } else {
            c();
          }
        });
      } catch (err) {
        console.error(err);
        e(err);
      }
    });
  });
}
```

There's some `run` function defined here. Hmm

And then `mocha` with `tdd`. I don't know what means. I can just see that it is
one of the options for Mocha, when invoking the constrcutor

```typescript
const mocha = new Mocha({
  ui: "tdd",
  color: true,
});
```

```typescript
/**
 * Mocha API
 *
 * @see https://mochajs.org/api/mocha
 */
declare class Mocha {
    private _growl;
    private _reporter;
    private _ui;

    constructor(options?: Mocha.MochaOptions);
```

```typescript
    /**
     * Options to pass to Mocha.
     */
    interface MochaOptions {
        /** Test interfaces ("bdd", "tdd", "exports", etc.). */
        ui?: Interface;
        //...
        //...
                /** Color TTY output from reporter */
        color?: boolean;
```

And then there's

```typescript
const testsRoot = path.resolve(__dirname, "..");
```

Which points to the `test` directory

```typescript
return new Promise((c, e) => {
  glob("**/**.test.js", { cwd: testsRoot }, (err, files) => {
    if (err) {
      return e(err);
    }

    // Add files to the test suite
    files.forEach((f) => mocha.addFile(path.resolve(testsRoot, f)));

    try {
      // Run the mocha test
      mocha.run((failures) => {
        if (failures > 0) {
          e(new Error(`${failures} tests failed.`));
        } else {
          c();
        }
      });
    } catch (err) {
      console.error(err);
      e(err);
    }
  });
});
```

It then finds out all the test files with the name ending with `.test.js`. Note
how it is NOT `.ts` but `js`. Hmm

And then it adds the files to mocha and then runs it. If there are any failures
it shows it. That's it! :)

Finally there's the extension test `extension.test.ts`

```typescript
import * as assert from "assert";

// You can import and use all API from the 'vscode' module
// as well as import your extension to test it
import * as vscode from "vscode";
// import * as myExtension from '../../extension';

suite("Extension Test Suite", () => {
  vscode.window.showInformationMessage("Start all tests.");

  test("Sample test", () => {
    assert.equal(-1, [1, 2, 3].indexOf(5));
    assert.equal(-1, [1, 2, 3].indexOf(0));
  });
});
```

I tried to run the same tests in VS Code and it just worked!! :O I ran the
`Extension Tests` launch configuration.

I think I know why the command line thing is not working - I can see a lot of
extensions getting activated when running from command line and some error
occurs there I think. Not sure. Hmm.

There is also a good explanation here

https://code.visualstudio.com/api/working-with-extensions/testing-extension

So, it uses the `vscode-test` library -
https://github.com/microsoft/vscode-test

Okay, I was able to disable all the other extensions and things worked! :)

Like this

```typescript
await runTests({
  extensionDevelopmentPath,
  extensionTestsPath,
  launchArgs: ["--disable-extensions"],
});
```

```bash
$ npm run test

> karuppiah@0.0.1 pretest /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/vscode-extension-experiments/karuppiah
> npm run compile && npm run lint


> karuppiah@0.0.1 compile /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/vscode-extension-experiments/karuppiah
> tsc -p ./


> karuppiah@0.0.1 lint /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/vscode-extension-experiments/karuppiah
> eslint src --ext ts


> karuppiah@0.0.1 test /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/vscode-extension-experiments/karuppiah
> node ./out/test/runTest.js

Found .vscode-test/vscode-1.50.1. Skipping download.
Warning: 'sandbox' is not in the list of known options, but still passed to Electron/Chromium.

[main 2020-10-17T16:44:40.653Z] update#setState idle

nvm is not compatible with the "npm_config_prefix" environment variable: currently set to "/Users/karuppiahn/.nvm/versions/node/v12.19.0"

Run `unset npm_config_prefix` to unset it.

(node:71133) Electron: Loading non-context-aware native module in renderer: '/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/vscode-extension-experiments/karuppiah/.vscode-test/vscode-1.50.1/Visual Studio Code.app/Contents/Resources/app/node_modules.asar.unpacked/vscode-sqlite3/build/Release/sqlite.node'. This is deprecated, see https://github.com/electron/electron/issues/18397.

2020-10-17 22:14:42.246 Code Helper (Renderer)[71133:1354655] CoreText note: Client requested name ".NewYork-Regular", it will get Times-Roman rather than the intended font. All system UI font access should be through proper APIs such as CTFontCreateUIFontForLanguage() or +[NSFont systemFontOfSize:].
2020-10-17 22:14:42.246 Code Helper (Renderer)[71133:1354655] CoreText note: Set a breakpoint on CTFontLogSystemFontNameRequest to debug.

2020-10-17 22:14:42.253 Code Helper (Renderer)[71133:1354655] CoreText note: Client requested name ".NewYork-Regular", it will get Times-Roman rather than the intended font. All system UI font access should be through proper APIs such as CTFontCreateUIFontForLanguage() or +[NSFont systemFontOfSize:].

(node:71133) Electron: Loading non-context-aware native module in renderer: '/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/vscode-extension-experiments/karuppiah/.vscode-test/vscode-1.50.1/Visual Studio Code.app/Contents/Resources/app/node_modules.asar.unpacked/spdlog/build/Release/spdlog.node'. This is deprecated, see https://github.com/electron/electron/issues/18397.

(node:71330) Electron: Loading non-context-aware native module in renderer: '/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/vscode-extension-experiments/karuppiah/.vscode-test/vscode-1.50.1/Visual Studio Code.app/Contents/Resources/app/node_modules.asar.unpacked/spdlog/build/Release/spdlog.node'. This is deprecated, see https://github.com/electron/electron/issues/18397.



  Extension Test Suite

    ✓ Sample test
  1 passing (11ms)

Exit code:   0
Done
```

https://code.visualstudio.com/api/working-with-extensions/testing-extension#disabling-other-extensions-while-debugging

And about VS Code limitation of not being able to run tests if VS Code stable is
already running -

https://code.visualstudio.com/api/working-with-extensions/testing-extension#using-insiders-version-for-extension-development

It says I can use VS Code Insiders for development. I think what they mean is -
use Insiders for writing the extension but for running tests, it will use the
one and only stable VS Code running. Hmm. Nice. Initially I misunderstood it as
use Insiders and imagined that has the ability to run the tests from CLI in
Insiders when there is another Insiders instance running. Hmm.

This seems to be a valid reason to use Insiders! :) I used to use it a bit and
then got rid of it since I saw no particular use of it. Maybe this is a good
use case!! :)

I install insiders build :)

And also found out how to install `code-insider` command line tool in the
terminal in PATH. Could have done it manually as I checked what `code` pointed
to

```bash
$ which code
/usr/local/bin/code
$ ls -l /usr/local/bin/code
lrwxr-xr-x  1 karuppiahn  admin  68 Mar 11  2018 /usr/local/bin/code -> /Applications/Visual Studio Code.app/Contents/Resources/app/bin/code
```

I just had to use the command palette and install it. And it can be done even
from VS Code stable it seems! :)

https://github.com/Microsoft/vscode/issues/6627
https://stackoverflow.com/questions/29963617/how-to-call-vs-code-editor-from-terminal-command-line

Now with insiders for doing development, I can run VS Code integration
tests easily. Or just close VS Code stable and run them. As I need to
really configure my Insiders. I guess it's high time I start exporting
all my configuration and settings and themes. I do have some of my
settings I think, using some settings sync extension. I think now it's
present in VS Code by default or something. Anyways.

---

I really need to note down all the editor extensions and the CLI tools
that are needed for the development. I think CLI tools will be taken
care of by `npm`. For extensions, I can use recommendations. Currently
there is this

```json
{
  // See http://go.microsoft.com/fwlink/?LinkId=827846
  // for the documentation about the extensions.json format
  "recommendations": ["dbaeumer.vscode-eslint"]
}
```

So yeah. I can add Prettier and what not :)

---

There's also some recommendation on how to do custom setup in case it
comes up later -

https://code.visualstudio.com/api/working-with-extensions/testing-extension#custom-setup-with-vscodetest

For integration test ;)

---

I'm wondering how to run these integration tests in CI pipeline though?!
As there's no UI there or VS Code instance. Hmm. I think there's a page
for that! :)

https://code.visualstudio.com/api/working-with-extensions/continuous-integration

Yup!

So we can run on any CI service. They have of course Azure as an example as
they are microsoft. Anyways. I don't think the Azure pipelines are free.
I think Travis or similar is good for now! :)

https://travis-ci.org/github/microsoft/vscode-test/builds/731847123/config

https://github.com/microsoft/vscode-test/blob/master/.travis.yml

```yaml
language: node_js
os:
  - osx
  - linux
node_js: 8

install:
  - |
    if [ $TRAVIS_OS_NAME == "linux" ]; then
      export DISPLAY=':99.0'
      /usr/bin/Xvfb :99 -screen 0 1024x768x24 > /dev/null 2>&1 &
    fi
script:
  - |
    echo ">>> Compile vscode-test"
    yarn && yarn compile
    echo ">>> Compiled vscode-test"
    cd sample
    echo ">>> Run sample integration test"
    yarn && yarn compile && yarn test
cache: yarn
```

https://github.com/microsoft/vscode-test/blob/master/sample/azure-pipelines.yml

Oh. Azure pipelines has support for open source projects it seems :O wow. Hmm.
And it can run for many operating systems. Hmm. Something that's fancy, as I
want to support Mac, Linux AND windows. And unlimited minutes and 10 parallel
jobs? Hmm. Something to try ! :)

Looks like GitHub actions can support that too!

There's some `xvfb` stuff to do. I don't know. But clearly it is running
with UI and stuff, looking at the screen config etc.

But I can't be using GitHub Actions as I'm on GitLab. So that's off!
Travis CI, or others, and then Azure pipelines. Hmm. I would love to
see speed in my pipelines. Let's see.

Travis CI supports windows too
https://docs.travis-ci.com/user/reference/windows/
Cool!! :)

Not sure which windows version. I'll come back to it later. Hmm.

---

`vsce` is the command to be used for publishing extensions automatically

```bash
npx vsce --help
npx: installed 67 in 10.186s
Usage: vsce <command> [options]

Options:
  -V, --version                        output the version number
  -h, --help                           display help for command

Commands:
  ls [options]                         Lists all the files that will be published
  package [options]                    Packages an extension
  publish [options] [<version>]        Publishes an extension
  unpublish [options] [<extensionid>]  Unpublishes an extension. Example extension id: microsoft.csharp.
  ls-publishers                        List all known publishers
  create-publisher <publisher>         Creates a new publisher
  delete-publisher <publisher>         Deletes a publisher
  login <publisher>                    Add a publisher to the known publishers list
  logout <publisher>                   Remove a publisher from the known publishers list
  show [options] <extensionid>         Show extension metadata
  search [options] <text>              search extension gallery
  help [command]                       display help for command
```

---

Publishing extension

https://code.visualstudio.com/api/working-with-extensions/publishing-extension#create-a-publisher

```bash
$ npm i -g vsce
```

I'm creating an Azure DevOps Organization for my extension to create a personal
access token (PAT) for publishing extensions later ;)

Org name is `SnappingShrimp`

I created a project too `cocreate-remote-vscode`

https://dev.azure.com/SnappingShrimp/cocreate-remote-vscode

Okay, access token creation alone is left. Hmm

---

I can simply package and install extensions too! :D

```bash
$ vsce package
 ERROR  Missing publisher name. Learn more: https://code.visualstudio.com/api/working-with-extensions/publishing-extension#publishing-extensions
```

For now, I need to add more details to the code.

https://code.visualstudio.com/api/working-with-extensions/publishing-extension#packaging-extensions

---

Apparently, for better installation and running of extensions, it's better to
bundle extensions - minify, and bundle source into one file I guess

https://code.visualstudio.com/api/working-with-extensions/bundling-extension

---

Compatibility with VS Code, hmm

https://code.visualstudio.com/api/working-with-extensions/publishing-extension#visual-studio-code-compatibility

---

Marketplace beautification

https://code.visualstudio.com/api/working-with-extensions/publishing-extension#marketplace-integration

---

VS Code Ignore to ignore unnecessary files from the extension I guess. For
example tests?

https://code.visualstudio.com/api/working-with-extensions/publishing-extension#.vscodeignore

---

Packaging finally worked when I actually added the publisher ID `SnappingShrimp`
as the publisher name. Apparently it's a unique thing, and extension is IDed
with publisher name and extension name together.

https://code.visualstudio.com/api/references/extension-manifest

```bash
$ vsce package
Executing prepublish script 'npm run vscode:prepublish'...

> karuppiah@0.0.1 vscode:prepublish /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/vscode-extension-experiments/karuppiah
> npm run compile


> karuppiah@0.0.1 compile /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/vscode-extension-experiments/karuppiah
> tsc -p ./

 WARNING  A 'repository' field is missing from the 'package.json' manifest file.
Do you want to continue? [y/N] y
 DONE  Packaged: /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/vscode-extension-experiments/karuppiah/karuppiah-0.0.1.vsix (7 files, 11.26KB)
```

---

When I need to use Icons

https://code.visualstudio.com/api/references/icons-in-labels
